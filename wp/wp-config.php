<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bellow');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@sn0Sey>-/{r9W{XY93/Q%]Rrs^n}r7|^H6_q1Qx@wkb?g%qbC>CnPc#-4,-Sp@ ');
define('SECURE_AUTH_KEY',  'j]t2#+{YUjw!H<:W(U= TE4c~0.HS[@E$yQtUi$JW+Z+MjSy|OfM/ocE#$z*3X/l');
define('LOGGED_IN_KEY',    'VAf]}kerD@ <G$y$!H&PS<IPcpIuv!6[C1whO_^d=+9B?v Q=w-O:m$_SZv,Gf1G');
define('NONCE_KEY',        ' z-n80B-7SuS.97641{|YG]dij918+mAI~3i40S!myK-GeAFKq%PSo?Zl6tn;xps');
define('AUTH_SALT',        '0FkC>(_x)j]e=?aANe$,4k_<XfWN;N&6O]fE2xqE1mVFjs|UrhQ c <I7?|^lWl5');
define('SECURE_AUTH_SALT', '1:>9WPu$mT$`eq{yaSXs gCov-NGTA<hLtI|As#KE;d8>%R_v-2f{?p+90ly:p!O');
define('LOGGED_IN_SALT',   'C_)aBd}N0S~AO#~x^2^T7Tvj/tMEHb$UT8RZLKg:9+jVmNma!LS]{7+r+N,DBfYI');
define('NONCE_SALT',       '8q}tA4wM0/9SktgM7VWU?>*{N%q.7+<U*cEYUH=kB0YN!:1Z|EF!GXOVCQxE-we2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'bl_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
