<?php
/**
 * Initialize the custom theme options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  
  /* OptionTree is not loaded yet */
  if ( ! function_exists( 'ot_settings_id' ) )
    return false;
    
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array( 
    'contextual_help' => array( 
      'sidebar'       => ''
    ),
    'sections'        => array( 

        array(
            'id'          => 'section_contact',
            'title'       => __( 'Contact Info', 'theme-options' )
        ),
        array(
            'id'          => 'section_footer',
            'title'       => __( 'Footer', 'theme-options' )
        ),        
        array(
            'id'          => 'section_social',
            'title'       => __( 'Social Links', 'theme-options' )
        ),        
        array(
            'id'          => 'section_microdata',
            'title'       => __( 'Microdata', 'theme-options' )
        ),
      
    ),
    'settings'        => array( 
      // ====== History ======    
 
      // ====== CONTACT ======
      array(
        'id'          => 'contact_adrress',
        'label'       => __( 'Adrress', 'theme-options' ),        
        'type'        => 'text',
        'section'     => 'section_contact',        
        'operator'    => 'and'
      ),
      array(
        'id'          => 'contact_email',
        'label'       => __( 'Email', 'theme-options' ),        
        'type'        => 'text',
        'section'     => 'section_contact',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'contact_phone',
        'label'       => __( 'Phone', 'theme-options' ),        
        'type'        => 'text',
        'section'     => 'section_contact',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'contact_fax',
        'label'       => __( 'Fax', 'theme-options' ),        
        'type'        => 'text',
        'section'     => 'section_contact',
        'operator'    => 'and'
      ),
      // ====== FOOTER ======
      array(
        'id'          => 'footer_left',
        'label'       => __( 'Text on left side', 'theme-options' ),     
        'type'        => 'textarea',
        'section'     => 'section_footer',        
        'operator'    => 'and'
      ),
      array(
        'id'          => 'left_page_link',
        'label'       => __( 'Page on left', 'theme-options' ),
        'desc'        => __( 'Select the page link to', 'theme-options' ),        
        'type'        => 'page-select',
        'section'     => 'section_footer',        
        'operator'    => 'and'
      ),
      array(
        'id'          => 'right_page_link',
        'label'       => __( 'Page on right', 'theme-options' ),
        'desc'        => __( 'Select the page link to', 'theme-options' ),        
        'type'        => 'page-select',
        'section'     => 'section_footer',        
        'operator'    => 'and'
      ),
      // ====== SOCIAL ======  
      array(
        'id'          => 'media_facebook',
        'label'       => __( 'Facebook', 'theme-options' ),        
        'type'        => 'text',
        'section'     => 'section_social',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'media_twitter',
        'label'       => __( 'Twitter', 'theme-options' ),        
        'type'        => 'text',
        'section'     => 'section_social',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'media_instagram',
        'label'       => __( 'Instagram', 'theme-options' ),        
        'type'        => 'text',
        'section'     => 'section_social',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'media_yelp',
        'label'       => __( 'Yelp', 'theme-options' ),        
        'type'        => 'text',
        'section'     => 'section_social',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'microdata_name',
        'label'       => __( 'Business Name', 'theme-options' ),        
        'type'        => 'text',
        'section'     => 'section_microdata',        
        'operator'    => 'and'
      ),
      array(
        'id'          => 'microdata_address',
        'label'       => __( 'Street Address', 'theme-options' ),        
        'type'        => 'text',
        'section'     => 'section_microdata',        
        'operator'    => 'and'
      ),
      array(
        'id'          => 'microdata_city',
        'label'       => __( 'City', 'theme-options' ),        
        'type'        => 'text',
        'section'     => 'section_microdata',        
        'operator'    => 'and'
      ),
      array(
        'id'          => 'microdata_phone',
        'label'       => __( 'Phone', 'theme-options' ),        
        'type'        => 'text',
        'section'     => 'section_microdata',        
        'operator'    => 'and'
      ),
      array(
        'id'          => 'microdata_email',
        'label'       => __( 'Email', 'theme-options' ),        
        'type'        => 'text',
        'section'     => 'section_microdata',        
        'operator'    => 'and'
      )
      
    )
  );
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
  /* Lets OptionTree know the UI Builder is being overridden */
  global $ot_has_custom_theme_options;
  $ot_has_custom_theme_options = true;
  
}