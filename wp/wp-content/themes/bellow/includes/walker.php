<?php  
class UL_Class_Walker extends Walker_Nav_Menu {

  function start_lvl(&$output, $depth = 0, $args = array()) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\"submenu dropdown-menu level-".$depth."\">\n";
  }
}
?>