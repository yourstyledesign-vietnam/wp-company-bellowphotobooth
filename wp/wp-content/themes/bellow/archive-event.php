<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page' );
}
?>
<?php get_header(); ?>
<header class="page-header with-text">
    <div class="txt">
        <header class="main-title sm-gap show-on-load trans-slow shown animated fadeInDown">
          <?php _e("Gallery"); ?>
        </header>
        <div style="max-width: 500px; margin: 0 auto 30px;">
          <?php dynamic_sidebar('home-primary') ?>
        </div>
    </div><!-- txt -->
</header>
<div id="main" role="main">
  
  <?php if (have_posts()) : ?>
    <div class="section gallery-zone">
      
      <div class="g-list clearfix">
      <?php while (have_posts()) : the_post(); ?>
        <div <?php post_class('g-item') ?> id="post-<?php the_ID(); ?>">
          <a class="g-link-cover" href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
            <?php if (get_field('cover_image')): ?>
              <span class="g-cover-img">
                <img src="<?php the_field('cover_image') ?>" alt="<?php the_title_attribute(); ?>">
              </span>
            <?php else: ?>
              <span class="g-cover-img">
                <img src="http://placehold.it/400x400" alt="<?php the_title_attribute(); ?>">
              </span>
            <?php endif ?>
            <span class="g-title">
              <?php the_title(); ?>
            </span>
          </a>
        </div> <!-- g-item -->
      <?php endwhile; ?>    
      </div><!-- g-list -->
    </div><!-- gallery-zone -->
    <?php wp_pagenavi(); ?>
  <?php endif; ?>
 
</div>
<?php get_footer(); ?>