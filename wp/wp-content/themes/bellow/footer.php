<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page' );
}
?>
  <div class="post-nav">
    
      <div role="navigation" class="navigation post-navigation">
          <?php  
            $page_left = ot_get_option('left_page_link');
            $page_right = ot_get_option('right_page_link');
          ?>
          <?php if ( !empty($page_left) ): ?>

          <a rel="next" href="<?php echo get_permalink(icl_object_id($page_left,'post')); ?>">
          <span title="<?php echo get_the_title( $page_left ); ?>" class="prev-link trans">
            <span class="inner">
            <span class="icomoon">&#xea44;</span>
            <span class="letters trans "><?php echo get_the_title(icl_object_id($page_left,'post')); ?></span></span>
          </span>
          </a>
          <?php endif; ?>      

          <?php if ( !empty($page_right) ): ?>
          <a rel="prev" href="<?php echo get_permalink( icl_object_id($page_right,'post') ); ?>">
            <span title="<?php echo get_the_title($page_right); ?>" class="next-link trans">
              <span class="inner">
              <span class="icomoon">&#xea42;</span>
              <span class="letters trans "><?php echo get_the_title(icl_object_id($page_right,'post')); ?></span>
              </span>
            </span>
          </a>
          <?php endif; ?>      
          <div class="clear"></div>
      </div><!-- navigation -->

  </div><!-- post-nav -->

  <div class="footer">
    <div class="footer-inner">
      <div class="container">       
        <div class="copyright-text">
           Bellow Photo Booth © <?php echo date('Y') ?>
           <a class="top-page" href="#top"><span class="icon-circle-up"></span></a>
        </div> <!-- copyright -->       
      </div> <!-- container -->
    </div> <!-- / footer-inner -->
  </div> <!-- / footer -->

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-66172262-1', 'auto');
    ga('send', 'pageview');

  </script>
 
			   
  <?php wp_footer(); ?>

</body>
</html>
