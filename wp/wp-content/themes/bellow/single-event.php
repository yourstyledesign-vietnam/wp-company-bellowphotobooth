<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page' );
}
?>
<?php get_header(); ?>
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <header class="single-post-header with-text">
        <div class="txt">
            <h2 class="text-title dark animated fadeInDown">
                <a href="<?php echo get_post_type_archive_link('event' ); ?>"><?php _e("Event" ); ?></a>
              </h2>
            <h1 class="main-title sm-gap show-on-load trans-slow shown animated fadeInDown">
              <?php the_title(); ?>
            </h1>
        </div><!-- txt -->
        <div class="container">
          <div class="p-n-articles row">
              <div class="p-article col-xs-6">
                <div class="pna-title"><?php _e("Previous event" ); ?></div>
                <?php previous_post_link('%link') ?>
              </div>
              <div class="n-article col-xs-6">
                <div class="pna-title"><?php _e("Next event" ); ?></div>
                <?php next_post_link('%link') ?>
              </div>
              <div class="clearfix"></div>
          </div><!-- p-n-articles -->
        </div>
    </header>

    <div <?php post_class('single-post-container') ?> id="post-<?php the_ID(); ?>">
      <div class="contact-wrapper">
        <div class="container">        
          <?php the_content(); ?>   
        </div>
      </div><!-- contact-wrapper -->
      <?php if ( ! post_password_required() ) : ?>

           <?php if (have_rows('gallery')): ?>
              <div class="outside clearfix">
                <div class="offer-event-details animated fadeIn">
                  <aside class="share-holder">
                      <meta content="<?php echo $GLOBALS["TEMPLATE_RELATIVE_URL"] ?>screenshot.png" name="og:image">

                  <span class="share-link">
                      <span class="share-title"><?php _e("SHARE THIS", "devinition" ); ?></span>
                      <span class="share-icons trans-med">
                          <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"                target="_blank" title="<?php esc_html_e('Share on Facebook', 'devinition'); ?>" class="icon">
                            <span class="letters"></span>
                            <span tabindex="20" st_image="<?php echo get_stylesheet_directory_uri(); ?>/screenshot.png" st_url="<?php the_permalink(); ?>" st_title="<?php the_title(); ?> <?php bloginfo('name'); ?>" displaytext="Facebook" class="st_facebook_custom" st_processed="yes">
                            </span>
                          </a>
                           <a href="https://twitter.com/share?url=<?php the_permalink(); ?>&amp;text=<?php echo urlencode(get_the_title()); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" title="<?php esc_html_e('Share on Twitter', 'devinition'); ?>" class="icon">
                            <span class="letters"></span>
                            <span st_via="harborsuites" tabindex="21" st_image="<?php echo get_stylesheet_directory_uri(); ?>/screenshot.png" st_url="<?php the_permalink(); ?>" st_title="<?php the_title(); ?> <?php bloginfo('name'); ?>" displaytext="Tweet" class="st_twitter_custom" st_processed="yes">
                            </span>
                          </a>
                         
                          <span class="clear"></span>
                      </span>
                  </span>
                </aside>
                </div> 
                <div class="clear"></div>
              <div class="section-gallery-detail">

              <?php while(have_rows('gallery')): the_row(); ?>
                <a class="fancybox" rel="group" href="<?php the_sub_field('gallery_image') ?>">
                <img src="<?php the_sub_field('gallery_image') ?>" alt="<?php the_title(); ?> - <?php bloginfo('name'); ?>">
                </a>
              <?php endwhile; ?>
              </div><!-- section-gallery-detail -->
              </div> <!-- outside -->
            <?php endif ?>      
      <?php endif; ?>   
      

    </div><!-- single-post-container -->
  <?php endwhile; endif; ?>
<?php get_footer(); ?>
