<?php
/*
Template Name: About Us
*/
?>
<?php get_header(); ?>
<?php if (have_posts()): while (have_posts()): the_post(); ?>
<div class="page-container">
	<?php if (get_field('show_banner_at_top')): ?>
		<div class="section  page-hero-zone" style="background-image:url(<?php echo get_field('banner') ?>);">			
		</div> <!-- home-slider -->	
	<?php endif; ?>
	
	<div class="container">
		<div class="page-about">
			<div class="page-hero-text animated fadeIn">
				<div class="letters">
				<?php the_title(); ?>
				</div>
			</div>
			<h2 class="intro-title animated flipInX">
				<?php the_field('page_description'); ?>				
			</h2>
			<div class="black-hr"></div>
			<div class="page-desc animated fadeIn">
				<?php the_content(); ?>
			</div><!-- page-desc -->

			<div class="offer-post-details animated fadeIn">
	            <aside class="share-holder">
	                <meta content="<?php echo $GLOBALS["TEMPLATE_RELATIVE_URL"] ?>screenshot.png" name="og:image">

					<span class="share-link">
					    <span class="share-title"><?php _e("SHARE THIS", "devinition" ); ?></span>
					    <span class="share-icons trans-med">
					    	<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"                target="_blank" title="<?php esc_html_e('Share on Facebook', 'devinition'); ?>" class="icon">
					       
					        	<span class="letters"></span>
					        	<span tabindex="20" st_image="<?php echo get_stylesheet_directory_uri(); ?>/screenshot.png" st_url="<?php the_permalink(); ?>" st_title="<?php the_title(); ?> <?php bloginfo('name'); ?>" displaytext="Facebook" class="st_facebook_custom" st_processed="yes"></span>
					
					        </a>
					        <a href="https://twitter.com/share?url=<?php the_permalink(); ?>&amp;text=<?php echo urlencode(get_the_title()); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" title="<?php esc_html_e('Share on Twitter', 'devinition'); ?>" class="icon">
					  
					        	<span class="letters"></span><span st_via="harborsuites" tabindex="21" st_image="<?php echo get_stylesheet_directory_uri(); ?>/screenshot.png" st_url="<?php the_permalink(); ?>" st_title="<?php the_title(); ?> <?php bloginfo('name'); ?>" displaytext="Tweet" class="st_twitter_custom" st_processed="yes"></span>
							</a>
					      
					        <span class="clear"></span>
					    </span>
					</span>
				</aside>
			</div>	
		</div><!-- page-about -->
		
	</div><!-- container -->
	
</div> <!-- page-content -->

<div class="section section-staff">
	<div class="container">
		<h3 class="text-rep-subtitle animated"><?php the_field('about_title') ?>
		<div class="divider move-middle"></div>
		</h3>		
		<div class="clear"></div>
		<?php if (have_rows('staffs')): ?>
			<div class="staff-list">
			<?php while(have_rows('staffs')): the_row(); ?>
				<div class="staff-item animated">
		            <div class="block-inner">
		            	<?php if (get_sub_field('staff_image')): ?>
		            		<img src="<?php echo get_sub_field('staff_image'); ?>" alt="<?php the_field('staff_name') ?> | <?php the_field('staff_description'); ?> ">
		            	<?php else: ?>
		            		<img src="http://placehold.it/300x300" alt="<?php the_field('staff_name') ?> | <?php the_field('staff_description'); ?> ">
		            	<?php endif ?>
		            	
		            	
		            </div><!-- block-inner -->
		            <div class="title-wrapper">
		                <header class="title black"><?php the_sub_field('staff_name') ?></header>       
		                <div class="divider dark"></div>                
		                <p><?php the_sub_field('staff_description'); ?></p>
		            </div><!--  title-wrapper -->
		        </div><!-- staff-item -->	  
			<?php endwhile; ?>
			</div> <!-- staff-list -->		
		<?php endif; ?>
	</div><!-- container -->
</div> <!-- section-staff -->
<?php endwhile; endif; ?>
<?php get_footer(); ?>