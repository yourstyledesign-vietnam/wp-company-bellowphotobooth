<?php
/*
Template Name: Partners
*/
?>
<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<header class="page-header with-text">
    <div class="txt">
        <header class="main-title sm-gap show-on-load trans-slow shown animated fadeInDown">
        	<?php the_title(); ?>
        </header>        
        <?php if ( get_field('short_description') ): ?>
        <div class="show-on-load trans-slow delay-4 shown animated fadeIn">
            <div class="divider darker bottom-spaced"></div>
            <p class="p1"><?php the_field('short_description'); ?></p>            
        </div>
        <?php endif; ?>
    </div><!-- txt -->
</header>
<?php if ( have_rows('repeater_partners')): ?>
	<div class="container">
		<section class="partners-list clearfix">
			

				<?php		
					while (have_rows('repeater_partners')): the_row();
					$h_name = get_sub_field('partner_name');
					$h_link = get_sub_field('partner_link');
					$h_image = get_sub_field('partner_image');
					
					if (empty($h_image)) {
						$h_image = "http://placehold.it/700x300";
					}
				?>

				    <div class="partner-item">
			        	<a target="_blank" href="<?php echo $h_link; ?>" class="partner-link" style="background-image:url('<?php echo $h_image; ?>')">		        		
			        		<span class="partner-name">
			            		<?php echo $h_name; ?>
			            	</span>
			            </a>
				    </div>		
				<?php endwhile; ?>
			
		</section>
	</div>
<?php endif; ?>

<?php endwhile; endif; ?>

<?php get_footer(); ?>