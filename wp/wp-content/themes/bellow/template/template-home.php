<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

<!-- home-slider -->
<?php  if( have_rows('hero_sliders') ): ?>
	<div class="section home-hero-zone">

		<ul class="home-hero-slider" data-duration="<?php echo get_field('speed'); ?>00">
	    <?php while( have_rows('hero_sliders') ): the_row(); ?>     
	    	<li style="background-image:url(<?php echo get_sub_field('sliders_image') ?>)"></li>
		<?php endwhile; ?>
		</ul>
		<div class="home-hero-text">
			<?php if (get_field('hero_title')): ?>
				<h1 class="animated fadeInDown"><?php the_field('hero_title') ?></h1>	
			<?php endif; ?>
			
			<?php if (get_field('hero_description')): ?>
				<h2 class="animated fadeInDown"><?php the_field('hero_description') ?></h2>	
			<?php endif ?>
			
		</div>

		<div class="home-next-slide"><span class="icomoon">&#xea42;</span></div>
		<div class="home-prev-slide"><span class="icomoon">&#xea44;</span></div>
		<div class="black-cover"></div>
	</div> <!-- home-slider -->
  <?php endif; ?> 
<!-- / home-slider -->

<!-- HOME GALLERY -->
<?php if (have_rows('events')): ?>
	<?php 		
		$i = 1;
	?>
	<div class="g-list clearfix">
	<?php while(have_rows('events')): the_row(); ?>
		<?php  
			$e_post = get_sub_field('events_event');
			$e_id = $e_post->ID;
			$class_name = "g-item";
			if ( $i == 9 || $i == 10 ) {
				$class_name = "g-item hidden-md";
			}
			$e_title = get_the_title($e_id);
			$e_img = get_field('cover_image',$e_id);
			$e_link = get_permalink( $e_id );
		?>
		<div <?php post_class($class_name) ?> id="post-<?php echo $e_id; ?>">
          <a class="g-link-cover" href="<?php echo $e_link; ?>" rel="bookmark" title="Permanent Link to <?php echo $e_title; ?>">
            <?php if (!empty($e_img)): ?>
              <span class="g-cover-img">
                <img src="<?php echo $e_img; ?>" alt="<?php echo $e_title; ?>">
              </span>
            <?php else: ?>
              <span class="g-cover-img">
                <img src="http://placehold.it/400x400" alt="<?php echo $e_title; ?>">
              </span>
            <?php endif ?>
            <span class="g-title">
              <?php echo $e_title; ?>
            </span>
          </a>
        </div> <!-- g-item -->
		<?php $i++; ?>
	<?php endwhile; ?>
	</div><!-- g-list -->
<?php endif; ?>
<!-- / HOME GALLERY -->

<!-- home-intro-zone -->
<?php if( have_rows('features') ): ?>
	<div class="section home-intro-zone">
		<div class="container">		
			<div class="intro-list">
				<div class="row home-intro-list">
				    <?php while( have_rows('features') ): the_row(); ?>     
						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 home-intro-item">
							<div class="intro-item animated">
								<?php if (get_sub_field('feature_image')): ?>
								<div class="intro-icon">
									<img src="<?php echo get_sub_field('feature_image') ?>" alt="<?php the_sub_field('feature_title') ?> - <?php bloginfo('name'); ?>" title="<?php the_sub_field('feature_title') ?> - <?php bloginfo('name'); ?>">
								</div>	
								<?php endif; ?>
								
								<div class="intro-txt">
									<?php if (get_sub_field('feature_title')): ?>
										<h4><?php the_sub_field('feature_title') ?></h4>	
									<?php endif; ?>
									
									<?php if ( get_sub_field('feature_description') ): ?>
										<div class="hight-light-txt-desc">
											<?php the_sub_field('feature_description') ?>
										</div>
									<?php endif; ?>
								</div>
							</div> <!-- intro-item -->					
						</div>				      
				  	<?php endwhile; ?>
				</div><!-- row -->
			</div> <!-- intro-list -->
		</div>
	</div> <!-- home-intro -->
<?php endif; ?> 

<!-- / home-intro-zone -->

<?php get_footer(); ?>