<?php
/*
Template Name: Our Booth
*/

?>
<?php get_header(); ?>

<?php  
	$frontpage_ID = get_option('page_on_front');
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<!-- home-slider -->
<?php  if( have_rows('hero_sliders', $frontpage_ID) ): ?>
	<div class="section home-hero-zone">

		<ul class="home-hero-slider" data-duration="<?php echo get_field('speed',$frontpage_ID); ?>00">
	    <?php while( have_rows('hero_sliders', $frontpage_ID) ): the_row(); ?>     
	    	<li style="background-image:url(<?php echo get_sub_field('sliders_image',$frontpage_ID) ?>)"></li>
		<?php endwhile; ?>
		</ul>
		<div class="home-hero-text">
			
			<h1 class="animated fadeInDown"><?php the_title(); ?></h1>	
			<?php if (get_field('core_benefit')): ?>
				<h2 class="animated fadeInDown"><?php the_field('core_benefit') ?></h2>	
			<?php endif ?>
			
		</div>

		<div class="home-next-slide"><span class="icomoon">&#xea42;</span></div>
		<div class="home-prev-slide"><span class="icomoon">&#xea44;</span></div>
		<div class="black-cover"></div>
	</div> <!-- home-slider -->
  <?php endif; ?> 
<!-- / home-slider -->

<!-- OUR BOOTH -->
<?php if( have_rows('booths') ): ?>
	
    <?php
	    $i = 1;    
	    while( have_rows('booths') ): the_row(); 
    ?>     
      
        <div class="text-rep-block-2-holder animated <?php if ($i%2 == 0) { echo 'alt'; } ?>">
			<article class="text-rep-block-2">
		        <div class="text-rep-thumb-holder" >
		            <div class="text-rep-thumb">
		                <div class="number">0<?php echo $i; ?></div>
		            </div><!-- text-rep-thumb -->
		        </div><!-- text-rep-thumb-holder -->

		        <div class="rep-text">
		            <div class="rep-main">
		                <div class="rep-main-inner">
		                	<?php if (get_sub_field('booth_name')): ?>
		                		<header><h2 class="text-rep-title"><?php the_sub_field('booth_name') ?></h2></header>	
		                	<?php endif ?>		                    
		                   	<div class="divider move-left"></div>
		                    <div>
		                    	<?php if ( get_sub_field('booth_description')): ?>
		                    		<?php the_sub_field('booth_description') ?>	
		                    	<?php endif ?>
		                    </div>
		                </div><!-- rep-main-inner -->
		            </div><!-- rep-main -->
		            <?php if (get_sub_field('booth_image')): ?>
		            	<div class="rep-image">
		                	<img alt="<?php the_sub_field('booth_name') ?> - <?php bloginfo('name'); ?>" title="<?php the_sub_field('booth_name') ?> - <?php bloginfo('name'); ?>" src="<?php echo get_sub_field('booth_image') ?>">
		            	</div><!-- rep-image -->	
		            <?php endif ?>
		            
		            <div class="clear"></div>
		        </div><!-- rep-text -->
		        <div class="clear"></div>
		    </article>
        </div> <!-- text-rep-block -->
      
  	<?php 
	    $i++;
	    endwhile;
   	?>
<?php endif; ?> 
<!-- / OUR BOOTH -->
<!-- LAYOUT -->
<section class="grey-block sm-gap animated">
    <div class="text-holder">
        <header>
        	<?php if (get_field('layout_title')): ?>
        		<h1 class="main-title sm-gap"><?php the_field('layout_title') ?></h1>	
        	<?php endif ?>
            
            <?php if (get_field('layout_description')): ?>
            <h2 class="text-small-title"><?php the_field('layout_description') ?></h2>
            <?php endif ?>
        </header>       
    </div>
    <?php if (have_rows('list_of_layout')): ?>
    	<div class="container">
    		<?php $i = 1; ?>
	    	<section class="text-numbered-cols row">
	    	<?php while(have_rows('list_of_layout')): the_row(); ?>
	    		<?php 
	    			$class_name = "col-sm-4 col-md-4 col-lg-4";
	    			if (get_sub_field('display_size') == "small") {
	    				$class_name = "col-sm-6 col-md-6 col-lg-3";
	    			}
	    		?>
	    		<article class="col-xs-12 <?php echo $class_name; ?> animated fadeIn" >
	            	<div class="block-inner">
		                <div class="numeric-title black">0<?php echo $i ?></div>
		                
		                <img src="<?php echo get_sub_field('layout_image'); ?>" alt="<?php echo get_field('layout_description') ?>">     
	                </div>           
	        	</article>
	    		<?php $i++; ?>
	    	<?php endwhile; ?>
	    	</section>
    	</div><!-- container -->
    <?php endif; ?>   
</section>
<!-- / LAYOUT -->
<?php endwhile; endif; ?>   
<?php get_footer(); ?>