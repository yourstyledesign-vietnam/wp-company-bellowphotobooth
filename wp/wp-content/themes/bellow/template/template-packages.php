<?php
/*
Template Name: Packages
*/
?>
<?php get_header(); ?>
<?php  
	$frontpage_ID = get_option('page_on_front');	
?>
<!-- home-slider -->
<?php  if( have_rows('hero_sliders', $frontpage_ID) ): ?>
	<div class="section home-hero-zone">

		<ul class="home-hero-slider" data-duration="<?php echo get_field('speed',$frontpage_ID); ?>00">
	    <?php while( have_rows('hero_sliders', $frontpage_ID) ): the_row(); ?>     
	    	<li style="background-image:url(<?php echo get_sub_field('sliders_image',$frontpage_ID) ?>)"></li>
		<?php endwhile; ?>
		</ul>
		<div class="slider-details-circle show-on-load trans-slower delay-2 mobile-wide-hidden shown   ">
	        <div class="details-inner  ">
	            <a class="link animated fadeIn" href="#assigned-offers"><?php the_field('text_1') ?></a>
	            <a href="#assigned-offers" class="view-offer trans scrollto animated fadeIn"><?php the_field('text_2') ?><span class="icon-arrow-down animated fadeIn"></span></a>
	        </div>
	    </div>

		<div class="home-next-slide"><span class="icomoon">&#xea42;</span></div>
		<div class="home-prev-slide"><span class="icomoon">&#xea44;</span></div>
		<div class="black-cover"></div>
	</div> <!-- home-slider -->
  <?php endif; ?> 
<!-- / home-slider -->
<?php if( have_posts()): while(have_posts()): the_post(); ?>
	

	<?php if ( get_field('use_package_list_layout') ): ?>
	 	<section id="assigned-offers" class="beige-pink-section">
	 		<div class="rooms-list clearfix">
	 			<?php if (have_rows('package_list')): $i = 1; while (have_rows('package_list')): the_row();?>
	 				<div class="block-33 tablet-p-100 room-glance trans-slowest delay-1">
	                    <article class="room-box">
	                        <div class="room-top-bg"></div>
	                        <div class="room-img">	                            
	                            <span class="room-number">0<?php echo $i; ?></span>
	                        </div>
	                        <header>
	                        	<h2 class="title in-room-box">
	                        		<?php the_sub_field('package_name'); ?>
	                        	</h2>
	                        </header>

	                        <div class="style-bullets">
	                           	<p><?php the_sub_field('package_description') ?></p> 
								<ul>
									<?php if (have_rows('package_features')): while(have_rows('package_features')): the_row(); ?>
										<li><?php the_sub_field('feature_name'); ?></li>
									<?php endwhile; endif; ?>
								</ul>
	                        </div>

	                        <div class="room-details-table">
	                            <div class="table-cell price">
	                                <?php the_sub_field('package_price'); ?>
	                            </div>
	                            <div class="clear"></div>
	                        </div><!-- room-details-table -->
	                    </article>
	                </div><!-- room-glance -->
	 			<?php $i++; endwhile; endif; ?>
	 			
	 		</div>
	 	</section>
	<?php else: ?>
		<section id="assigned-offers" class="beige-pink-section">
			<div class="rooms-list">
				<div class="container">
					<div class="row">
						<div class="col-md-offset-2 col-md-8 col-xs-offset-0 col-xs-12 col-lg-offset-2 col-lg-8">
				    <?php  
				    	$table = get_field( 'package_prices' );

						if ( $table ) {

						    echo '<div class="wrapper-table-responsive"><table class="table table-bordered table-prices">';

						        if ( $table['header'] ) {

						            echo '<thead>';

						                echo '<tr>';

						                    foreach ( $table['header'] as $th ) {

						                        echo '<th>';
						                            echo $th['c'];
						                        echo '</th>';
						                    }

						                echo '</tr>';

						            echo '</thead>';
						        }

						        echo '<tbody>';

						            foreach ( $table['body'] as $tr ) {

						                echo '<tr>';

						                    foreach ( $tr as $td ) {

						                        echo '<td>';
						                            echo $td['c'];
						                        echo '</td>';
						                    }

						                echo '</tr>';
						            }

						        echo '</tbody>';

						    echo '</table></div>';
						}
				    ?>
				    	</div>
				    </div> <!-- row -->
			    </div> <!-- container -->
		    </div>
		</section>
	<?php endif; ?>
	

<?php endwhile; endif; ?>
<?php get_footer(); ?>