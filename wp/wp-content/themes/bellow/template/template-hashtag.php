<?php
/*
Template Name: Hashtag Printing
*/
?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<header class="page-header with-text">
    <div class="txt">
        <header class="main-title sm-gap show-on-load trans-slow shown animated fadeInDown">
        	<?php the_title(); ?>
        </header>        
        <?php if ( get_field('short_description') ): ?>
        <div class="show-on-load trans-slow delay-4 shown animated fadeIn">
            <div class="divider darker bottom-spaced"></div>
            <p class="p1"><?php the_field('short_description'); ?></p>            
        </div>
        <?php endif; ?>
    </div><!-- txt -->
</header>
<?php if ( have_rows('hashtag_features')): ?>
	<section class="experiences-page-list clearfix">
	<?php
		$i = 1;
		while (have_rows('hashtag_features')): the_row();
		$h_name = get_sub_field('hashtag_name');
		$h_desc = get_sub_field('hashtag_description');
		$h_icon = get_sub_field('hashtag_icon');
		$h_image = get_sub_field('feature_image');

		if ( empty($h_icon) ) {
			$h_icon ="http://placehold.it/700x300";
		}
		if (empty($h_image)) {
			$h_image = "http://placehold.it/700x300";
		}
	?>
		<?php if ( ($i%2) == 0 ): ?>
		    <div class="block-50 inline tablet-portrait-100" >
		        <div class="experience-link wide" >
		            <img class="exp-image" alt="<?php echo $h_name; ?>" src="<?php echo $h_image; ?>">
		            <span style="background-image: url(<?php echo $h_image; ?>);" class="img trans-slowest"></span>            
		        </div>
		    </div>
		    <div class="block-50 inline resp-wide-100" >
		        <div class="experience-link normal">
		            <img class="exp-image" alt="<?php echo $h_name; ?>" src="<?php echo $h_icon; ?>">
		            <span style="background-image: url(<?php echo $h_icon; ?>);" class="img trans-slowest"></span>
		            <span class="content">
		                <span class="content-txt">
		                    <header class="category-title">0<?php echo $i; ?></header>
		                    <?php if ( !empty($h_name) ): ?>
		                    	<h3 itemprop="name" class="title black the-exp-title trans"><?php echo $h_name; ?></h3>	
		                    <?php endif; ?>
		                    
		                    <?php if ( !empty($h_desc) ): ?>
		                    	<span class="experience-link-text"><?php echo $h_desc; ?></span>
		                    <?php endif; ?>
		                </span>            
		            </span>            
		        </div>
		    </div>
		<?php else: ?>
			<div class="block-50 inline tablet-portrait-100 visible-xs" >
		        <div class="experience-link wide" href="">
		            <img class="exp-image" alt="<?php echo $h_name; ?>" src="<?php echo $h_image; ?>">
		            <span style="background-image: url(<?php echo $h_image; ?>);" class="img trans-slowest"></span>           
		        </div>
		    </div>
			<div class="block-50 inline resp-wide-100 " >
		        <div class="experience-link normal">
		            <img class="exp-image" alt="<?php echo $h_name; ?>" src="<?php echo $h_icon; ?>">
		            <span style="background-image: url(<?php echo $h_icon; ?>);" class="img trans-slowest"></span>
		            <span class="content">
		                <span class="content-txt">                  
		                    <header class="category-title">0<?php echo $i; ?></header>

		                    <?php if ( !empty($h_name) ): ?>
		                    	<h3 itemprop="name" class="title black the-exp-title trans"><?php echo $h_name; ?></h3>	
		                    <?php endif; ?>
		                    
		                    <?php if ( !empty($h_desc) ): ?>
		                    	<span class="experience-link-text"><?php echo $h_desc; ?></span>
		                    <?php endif; ?>
		                    
		                </span>               
		            </span>
		            
		        </div>
		    </div>
		    <div class="block-50 inline tablet-portrait-100 hidden-xs" >
		        <div class="experience-link wide" href="">
		            <img class="exp-image" alt="<?php echo $h_name; ?>" src="<?php echo $h_image; ?>">
		            <span style="background-image: url(<?php echo $h_image; ?>);" class="img trans-slowest"></span>           
		        </div>
		    </div>
		<?php endif ?>	
	<?php $i++; endwhile; ?>
	</section>
<?php endif; ?>

<?php endwhile; endif; ?>
<?php get_footer(); ?>