<?php
/*
Template Name: Contact
*/
?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<header class="page-header with-text">
    <div class="txt">
    	<?php if ( get_field('contact_title')): ?>
    		<header class="main-title sm-gap show-on-load trans-slow shown animated fadeInDown">
	        	<?php the_field('contact_title') ?>
	        </header>	
    	<?php endif; ?>
        
        <?php if ( get_field('contact_description')): ?>
        	<h2 class="text-title dark sm-gap show-on-load trans-slow delay-2 shown animated fadeInDown">
        		<?php the_field('contact_description') ?>
        	</h2>              
        <?php endif ?>       
    </div><!-- txt -->
</header>
<div class="contact-wrapper">
	<?php if (have_rows('contact_info')): ?>
		<div class="container">
        	<div class="row">
			<?php while ( have_rows('contact_info') ): the_row() ?>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                
	                <div class="contact-info animated fadeInUp">
	                    <div class="footer-title"><?php the_sub_field('location') ?></div>
	                    <div class="divider move-left"></div>
	                    <p>
	                        <b>T.</b> <a href="tel:<?php echo get_sub_field('phone') ?>"> <?php the_sub_field('phone') ?> </a>
	                        <br>              
	                        <b>E.</b> <a href="mailto:<?php echo get_sub_field('email') ?>"><?php the_sub_field('email') ?></a> 
	                    </p>
	                    <div class="divider move-left"></div>
	                    <?php if (get_sub_field('address')): ?>
	                    	<div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address">
		                        <?php the_sub_field('address') ?>	
	                    <?php endif ?>
	                    
	                </div>
	            </div>
			<?php endwhile; ?>
			</div>
	    </div>
	<?php endif; ?>
    
    <?php 
		$contact_s = get_field('contact_shortcode');		
		if ( get_field('contact_shortcode') ): ?>
    <div class="container">
        <div class="contact-content clearfix">           
                <div lang="en-US" dir="ltr" class="wpcf7" role="form">                	
                    <div class="screen-reader-response"></div>
                    <?php echo do_shortcode( $contact_s ); ?>
                </div>            
        </div><!-- contact-content -->
    </div><!-- container -->
    <?php endif; ?>
</div><!-- contact-wrapper -->


<?php endwhile; endif; ?>   
<?php get_footer(); ?>