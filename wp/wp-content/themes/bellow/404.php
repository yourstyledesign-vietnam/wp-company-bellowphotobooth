<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page' );
}
?>
<?php get_header(); ?>

  <div id="main" role="main" class="page-header with-text">
  	<div class="container">
	    <div class="txt">
	        <img class="img-404" alt="404" src="<?php echo get_stylesheet_directory_uri(); ?>/images/404.png">
	        <div class="divider darken"></div>
	        <header class="main-title sm-gap"><h1><?php _e("ERROR PAGE","devinition" ); ?></h1></header>
	        <h2 class="text-title dark"><?php _e("WE ARE SORRY, NO PAGE WAS FOUND","devinition" ); ?></h2>
	    </div>
	</div><!-- container -->
  </div>

<?php get_footer(); ?>