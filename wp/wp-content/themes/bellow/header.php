<?php
/**
 * @package MyStyle
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
       Remove this if you use the .htaccess -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="format-detection" content="telephone=no">    

    <title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
    <link href='http://fonts.googleapis.com/css?family=Tinos:400,700&subset=latin,vietnamese' rel='stylesheet' type='text/css'>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="icon" href="<?php echo $GLOBALS["TEMPLATE_RELATIVE_URL"]; ?>images/favicon.ico" type="image/x-icon">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    
    <?php 
      // = REGISTER STYLESHEET =
      wp_enqueue_style( "bootstrap", $GLOBALS["TEMPLATE_RELATIVE_URL"]."css/bootstrap.css" ); 
      wp_enqueue_style( "animate", $GLOBALS["TEMPLATE_RELATIVE_URL"]."css/animate.css" ); 
      wp_enqueue_style( "justifiedGallery", $GLOBALS["TEMPLATE_RELATIVE_URL"]."css/justifiedGallery.min.css" ); 
      wp_enqueue_style( "style", $GLOBALS["TEMPLATE_RELATIVE_URL"]."style.css" ); 
      wp_enqueue_style( "style-responsive", $GLOBALS["TEMPLATE_RELATIVE_URL"]."style-responsive.css" ); 
    ?>
    <?php 
      if (ICL_LANGUAGE_CODE == "vi") {
        wp_enqueue_style( "style-vietname", $GLOBALS["TEMPLATE_RELATIVE_URL"]."style-vietnam.css" );    
      }
    ?>
    <?php 

      if (ICL_LANGUAGE_CODE == "es") {
        wp_enqueue_style( "style-es", $GLOBALS["TEMPLATE_RELATIVE_URL"]."style-es.css" );    
      }
    ?>
    <?php 
      // = REGISTER JAVASCRIPT =
      wp_enqueue_script( "modernizr-js", $GLOBALS["TEMPLATE_RELATIVE_URL"]."js/vendor/modernizr-2.8.0.min.js" ); 
      wp_enqueue_script( "bootstrap-js", $GLOBALS["TEMPLATE_RELATIVE_URL"]."js/bootstrap.min.js" );
      wp_enqueue_script( "touchSwipe-js", "https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.16/jquery.touchSwipe.min.js",'jQuery','',true);
      wp_enqueue_script( "justifiedGallery-js", $GLOBALS["TEMPLATE_RELATIVE_URL"]."js/jquery.justifiedGallery.min.js",'','', true );
      wp_enqueue_script( "plugins-js", $GLOBALS["TEMPLATE_RELATIVE_URL"]."js/plugins.js",'','', true );
      wp_enqueue_script( "main-js", $GLOBALS["TEMPLATE_RELATIVE_URL"]."js/main.js",'','', true );
    ?>
    
    <!-- END of REGISTER JAVASCRIPT --> 

    <!-- Wordpress Head Items -->
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    
    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

  <!--[if lt IE 8]>
    <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
  <![endif]-->
  <!-- Page Loader -->        
  <div class="page-loader">
      <b class="spinner"><img src="<?php echo $GLOBALS["TEMPLATE_RELATIVE_URL"] ?>images/site-logo.png" alt="" /></b>
  </div>
  <!-- End Page Loader -->
  <div class="hidden">
      <div itemscope itemtype="http://schema.org/LocalBusiness">
      <div itemprop="name"><?php echo ot_get_option('microdata_name') ?></div>
      <div>Email: <span itemprop="email"><a href='mailto:<?php echo ot_get_option('microdata_email') ?>'><?php echo ot_get_option('microdata_email') ?></a></span></div>
      <div>Phone: <span itemprop="telephone"><?php echo ot_get_option('microdata_phone') ?></span></div>      
      <div>Url: <span itemprop="url"><a href='<?php echo bloginfo('url'); ?>'><?php echo bloginfo('url'); ?></a></span></div>

      <meta itemprop="openingHours"  style='display: none'  datetime="Mo,Tu,We,Th,Fr,Sa,Su 0-0" />
      <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address">
        <div itemprop="streetAddress"><?php echo ot_get_option('microdata_address'); ?></div>        
      </div>
    </div>
  </div> <!-- hidden -->
  <div class="header-zone">
    <div class="header-zone-inner">
      <div class="header-container">
        <div class="row">
          <div class="col-xs-2 col-sm-3 col-md-3 col-lg-3">
            <?php do_action('icl_language_selector'); ?>
          </div>

          <div class="col-xs-8 col-sm-6 col-md-6 col-lg-6">
            <div class="primary-nav">
              <!-- MENU -->
              <?php 
                if ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'primary-menu' ) ) {
                  $args = array(
                    'theme_location' => 'primary-menu',
                    'container'      => false,
                    'menu'           => 'primary-menu',                
                    'menu_class'     => 'ul-primary-nav',
                    'depth'          => 5,
                    'walker' => new UL_Class_Walker()
                  );
                  wp_nav_menu( $args );
                };  
              ?>  
              <!-- END of MENU -->              
            </div>
          </div>
          <div class="col-xs-2 col-sm-3 col-md-3 col-lg-3">
            <?php  
              $f = ot_get_option('media_facebook');
              $t = ot_get_option('media_twitter');
              $i = ot_get_option('media_instagram');
              $y = ot_get_option('media_yelp');
            ?>
            <div class="follow">
              <span class="follow-title hidden-md hidden-sm hidden-xs"><?php _e("Follow Us","devinition" ); ?></span>
              <a target="_blank" href="<?php echo $i ?>"><span class="letters">&#xea90;</span></a>
              <?php if (!empty($y)): ?>
                <a target="_blank" href="<?php echo $y ?>"><span class="letters"><i class="icon-yelp"></i></span></a>  
              <?php endif; ?>
              
              <a target="_blank" href="<?php echo $t; ?>"><span class="letters"></span></a>
              <a class="fb" target="_blank" href="<?php echo $f; ?>"><span class="letters"></span></a>
            </div>
          </div>
        </div> <!-- row -->
      </div> <!-- container -->
    </div> <!-- header-zone-inner -->
  </div> <!-- header -->



