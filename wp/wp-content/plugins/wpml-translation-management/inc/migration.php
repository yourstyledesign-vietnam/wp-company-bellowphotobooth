<?php
/**
 * @package wpml-tm
 */

if ( ! defined( 'ICL_SITEPRESS_VERSION' ) || ! defined( 'WPML_TM_VERSION' ) || ! defined( 'WPML_TM_VERSION' ) || ! is_admin() ) {
	return;
}

if(!function_exists('wpml_tp_upgrade_version')) {
	function wpml_tp_upgrade_version($version, $force = false)
	{
		if ($force || (get_option('wpml_tm_version') && version_compare(get_option('wpml_tm_version'), $version, '<'))) {
			$upg_file = WPML_TM_PATH . '/inc/migration/migration-' . $version . '.php';
			if (file_exists($upg_file) && is_readable($upg_file)) {
				if (!defined('WPML_DOING_UPGRADE')) {
					define('WPML_DOING_UPGRADE', true);
				}
				include_once $upg_file;
			}
		}
	}
}

if ( defined( 'WPML_TM_DEV_VERSION' ) ) {
	wpml_tp_upgrade_version( WPML_TM_VERSION, true );
}

if ( version_compare( get_option( 'wpml_tm_version' ), WPML_TM_VERSION, '<' ) ) {
	update_option( 'wpml_tm_version', WPML_TM_VERSION );
}
