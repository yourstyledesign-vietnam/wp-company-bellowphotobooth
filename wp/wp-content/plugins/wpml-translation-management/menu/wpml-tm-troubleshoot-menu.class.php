<?php

class WPML_TM_Troubleshoot_Menu {

    public static function display_tp_communication_options() {
        global $sitepress, $wpdb;

        $current_service_name = TranslationProxy::get_current_service_name();
        if ( SitePress_Setup::setup_complete() && !icl_do_not_promote() ) {
            $troubleshooting_options = $sitepress->get_setting( 'troubleshooting_options', array() );
            $raise_mysql_errors      = false;
            $http_communication      = false;
            if ( $troubleshooting_options ) {
                $raise_mysql_errors = isset( $troubleshooting_options[ 'raise_mysql_errors' ] )
                    ? $troubleshooting_options[ 'raise_mysql_errors' ] : false;
                $http_communication = isset( $troubleshooting_options[ 'http_communication' ] )
                    ? $troubleshooting_options[ 'http_communication' ] : false;
            }
            ?>
            <div class="icl_cyan_box">
                <h3 id="more-options"><?php _e( 'More options', 'sitepress' ) ?></h3>

                <form name="icl_troubleshooting_more_options" id="icl_troubleshooting_more_options" action="">
                    <?php wp_nonce_field( 'icl_troubleshooting_more_options_nonce', '_icl_nonce' ); ?>
                    <label><input type="checkbox" name="troubleshooting_options[raise_mysql_errors]" value="1" <?php
                        if ($raise_mysql_errors){ ?>checked="checked"<?php } ?>/>&nbsp;<?php
                        _e( 'Raise mysql errors on XML-RPC calls', 'sitepress' ) ?></label>
                    <br/>
                    <label><input type="checkbox" name="troubleshooting_options[http_communication]" value="1" <?php
                        if ($http_communication){ ?>checked="checked"<?php } ?>/>&nbsp;<?php
                        echo sprintf(
                            __( 'Communicate with %s using HTTP instead of HTTPS', 'sitepress' ),
                            $current_service_name
                        ); ?></label>

                    <p>
                        <input class="button" name="save" value="<?php echo __( 'Apply', 'sitepress' ) ?>"
                               type="submit"/>
                        <span class="icl_ajx_response" id="icl_ajx_response"></span>
                    </p>
                </form>
            </div>
            <br clear="all"/>


            <br clear="all"/>
            <?php if ( SitePress_Setup::setup_complete() && ( !defined(
                        'ICL_DONT_PROMOTE'
                    ) || !ICL_DONT_PROMOTE )
            ) { ?>
                <br/>
                <div class="icl_cyan_box">
                    <h3><?php _e( 'Reset PRO translation configuration', 'sitepress' ) ?></h3>

                    <div
                        class="icl_form_errors"><?php _e(
                            "Resetting your ICanLocalize account will interrupt any translation jobs that you have in progress. Only use this function if your ICanLocalize account doesn't include any jobs, or if the account was deleted.",
                            'sitepress'
                        ); ?></div>
                    <p style="padding:6px;"><label><input
                                onchange="if(jQuery(this).attr('checked')) jQuery('#icl_reset_pro_but').removeClass('button-primary-disabled'); else jQuery('#icl_reset_pro_but').addClass('button-primary-disabled');"
                                id="icl_reset_pro_check" type="checkbox" value="1"/>&nbsp;<?php _e(
                                'I am about to reset the ICanLocalize project setting.',
                                'sitepress'
                            ); ?></label></p>

                    <a id="icl_reset_pro_but"
                       onclick="if(!jQuery('#icl_reset_pro_check').attr('checked') || !confirm('<?php echo esc_js(
                           __( 'Are you sure you want to reset the PRO translation configuration?', 'sitepress' )
                       ) ?>')) return false;"
                       href="admin.php?page=<?php echo basename(
                           ICL_PLUGIN_PATH
                       ) ?>/menu/troubleshooting.php&amp;debug_action=reset_pro_translation_configuration&amp;nonce=<?php echo wp_create_nonce(
                           'reset_pro_translation_configuration'
                       ) ?>"
                       class="button-primary button-primary-disabled"><?php _e(
                            'Reset PRO translation configuration',
                            'sitepress'
                        ); ?></a>

                </div>

                <br clear="all"/>
            <?php } ?>


            <br clear="all"/>
            <?php if ( !defined( 'ICL_DONT_PROMOTE' ) || !ICL_DONT_PROMOTE ) { ?>
                <br/>
                <div class="icl_cyan_box">
                    <a name="icl-connection-test"></a>

                    <h3><?php _e( 'ICanLocalize connection test', 'sitepress' ) ?></h3>
                    <?php if ( isset( $_GET[ 'icl_action' ] ) && $_GET[ 'icl_action' ] == 'icl-connection-test' ) { ?>
                        <?php
                        $icl_query = new ICanLocalizeQuery();
                        if ( isset( $_GET[ 'data' ] ) ) {
                            $user = unserialize( base64_decode( $_GET[ 'data' ] ) );
                        } else {
                            $user[ 'create_account' ] = 1;
                            $user[ 'anon' ]           = 1;
                            $user[ 'platform_kind' ]  = 2;
                            $user[ 'cms_kind' ]       = 1;
                            $user[ 'blogid' ]         = $wpdb->blogid ? $wpdb->blogid : 1;
                            $user[ 'url' ]            = get_option( 'siteurl' );
                            $user[ 'title' ]          = get_option( 'blogname' );
                            $user[ 'description' ]    = $sitepress->get_setting( 'icl_site_description' )
                                ? $sitepress->get_setting( 'icl_site_description' ) : '';
                            $user[ 'is_verified' ]    = 1;
                            if ( defined( 'ICL_AFFILIATE_ID' ) && defined( 'ICL_AFFILIATE_KEY' ) ) {
                                $user[ 'affiliate_id' ]  = ICL_AFFILIATE_ID;
                                $user[ 'affiliate_key' ] = ICL_AFFILIATE_KEY;
                            }
                            $user[ 'interview_translators' ] = $sitepress->get_setting( 'interview_translators' );
                            $user[ 'project_kind' ]          = 2;
                            $user[ 'pickup_type' ]           = intval(
                                $sitepress->get_setting( 'translation_pickup_method' )
                            );
                            $notifications                   = 0;
                            if ( $sitepress->get_setting( 'icl_notify_complete' ) ) {
                                $notifications += 1;
                            }
                            if ( $sitepress->get_setting( 'alert_delay' ) ) {
                                $notifications += 2;
                            }
                            $user[ 'notifications' ]    = $notifications;
                            $user[ 'ignore_languages' ] = 0;
                            $user[ 'from_language1' ]   = isset( $_GET[ 'lang_from' ] ) ? $_GET[ 'lang_from' ]
                                : 'English';
                            $user[ 'to_language1' ]     = isset( $_GET[ 'lang_to' ] ) ? $_GET[ 'lang_to' ] : 'French';
                        }

                        define( 'ICL_DEB_SHOW_ICL_RAW_RESPONSE', true );
                        $resp = $icl_query->createAccount( $user );
                        echo '<textarea style="width:100%;height:400px;font-size:9px;">';
                        if ( defined( 'ICL_API_ENDPOINT' ) ) {
                            echo ICL_API_ENDPOINT . "\r\n\r\n";
                        }
                        echo __( 'Data', 'sitepress' ) . "\n----------------------------------------\n" .
                             print_r( $user, 1 ) .
                             __( 'Response', 'sitepress' ) . "\n----------------------------------------\n" .
                             print_r( $resp, 1 ) .
                             '</textarea>';

                        ?>

                    <?php } ?>
                    <a class="button"
                       href="admin.php?page=<?php echo ICL_PLUGIN_FOLDER ?>/menu/troubleshooting.php&ts=<?php echo time(
                       ) ?>&icl_action=icl-connection-test#icl-connection-test"><?php _e(
                            'Connect',
                            'sitepress'
                        ) ?></a>
                </div>
                <br clear="all"/>
            <?php } ?>
        <?php } ?>
    <?php
    }
}